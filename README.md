## Guide to Choosing a Full-Time Coding Bootcamp

### and find a sweet programming job

### By David Kim


__*Wait, I attended one of these schools! I know something!*__

Please write us with suggestions, corrections, or topics you might like to contribute! David <daviddarden11@gmail.com>.

Like on [Facebook/CodingBootcamp](https://www.facebook.com/CodingBootcamp)


### How can I contribute?
> If you want to contribute to this open-source documentation, try:

1. fork the repo

2. add upstream - `git remote add upstream git@github.com:dklounge/codingbootcamps.git`

3. create a branch for content - `git checkout -b my-new-section`

4. commit the change - `git commit -m 'added a new section about xyz'

5. push to the branch - `git push origin my-new-section`

6. create a new pull request (this is done through the repo on github.com)
